﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models
{
    public partial class Employees
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime? Dob { get; set; }
        public int? Salary { get; set; }
        public string Address { get; set; }
        public string ImageUrl { get; set; }
    }
}
