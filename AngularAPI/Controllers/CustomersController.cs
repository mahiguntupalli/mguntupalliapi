﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataAccess;
using Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AngularAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private DBTransactionContext _context;

        public CustomersController(DBTransactionContext context)
        {
            _context = context;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Customers>> Get()
        {
            var list = _context.Customers;
            return list;
        }

        // GET api/values/5
        [HttpGet("{customerId}")]
        public ActionResult<Customers> Get(int customerId)
        {
            return _context.Customers.Find(customerId);
        }
        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Customers customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.Customers.Add(customer);
            _context.SaveChanges();
            return Ok("Success!");
        }
        //PUT api/values/1
        [HttpPut]
        public IActionResult Put([FromBody] Customers customer)
        {
            // this will update the record
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.Customers.Update(customer);
            _context.SaveChanges();
            return Ok("Updated!");
        }

        // GET api/values/5
        //[HttpDelete("{customerId}")]
        //public IActionResult Delete(int customerId)
        //{
        //    try
        //    {
        //        var item = _context.Customers.Find(customerId);
        //        _context.Customers.Remove(item);
        //        _context.SaveChanges();

        //        return Ok("Deleted!");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        [HttpGet]
        [Route("GetCustomerInvoiceById")]
        public ActionResult<IEnumerable<CustomerInvoice>> GetCustomerInvoiceById(int customerId)
        {
            var result = (from c in _context.Customers
                              join d in _context.DailySheet on c.CustomerID equals d.CustomerID                              
                              where d.CustomerID == customerId
                              select new CustomerInvoice
                              {
                                  CustomerId = c.CustomerID,
                                  CustomerName = c.FirstName + " " + c.LastName,
                                  CreatedDate = d.CreatedDate.ToString(),
                                  MilkQuantity = d.MilkQuantity.ToString(),
                                  MilkFat = d.MilkFat.ToString(),
                                  Amount = d.Amount.ToString()
                              }).ToList();            
            return result;
        }

    }
}