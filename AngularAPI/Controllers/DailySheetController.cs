﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataAccess;
using Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AngularAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DailySheetController : ControllerBase
    {
        private DBTransactionContext _context;

        public DailySheetController(DBTransactionContext context)
        {
            _context = context;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<DailySheet>> Get()
        {
            var list = _context.DailySheet.Where(d => d.CreatedDate == DateTime.Today.ToLocalTime()).ToList();
            return list;
        }

        // GET api/values/5
        [HttpGet("{customerId}")]
        public ActionResult<DailySheet> Get(int customerId)
        {
            return _context.DailySheet.Find(customerId);
        }
        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] DailySheet dailySheet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.DailySheet.Add(dailySheet);
            _context.SaveChanges();
            return Ok("Success!");
        }

        [HttpGet]
        [Route("GetInvoiceByDates")]
        public ActionResult<IEnumerable<DailySheet>> GetInvoiceByDates(DateTime startDate, DateTime endDate)
        {
            var result = (from d in _context.DailySheet
                          where d.CreatedDate >= startDate && d.CreatedDate <=endDate.Date
                          select new DailySheet
                          {                              
                              CreatedDate = d.CreatedDate,
                              MilkQuantity = d.MilkQuantity,
                              MilkFat = d.MilkFat,
                              Amount = d.Amount
                          }).ToList();
            return result;
        }
    }
}