﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.Models
{
    public class DailySheet
    {
        [Key]
        public int SNo { get; set; }
        [Required]
        public int CustomerID { get; set; }
        [Required]
        public decimal MilkQuantity { get; set; }
        [Required]
        public decimal MilkFat { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
