﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using System.Web.Http.ModelBinding;
using Api.DataAccess;
using Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AngularAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class ProductsController : ControllerBase
    {

        private DBTransactionContext _context;

        public ProductsController(DBTransactionContext context)
        {
            _context = context;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Products>> Get()
        {
            var list = _context.Products;
            return list;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Products> Get(int id)
        {
            return _context.Products.Find(id);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Products product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.Products.Add(product);
            _context.SaveChanges();
            return Ok("Success!");
        }
        //PUT api/values/1
        [HttpPut]
        public IActionResult Put([FromBody] Products product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.Products.Update(product);
            _context.SaveChanges();
            return Ok("Updated!");
        }

        // GET api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var item = _context.Products.Find(id);
                _context.Products.Remove(item);
                _context.SaveChanges();
                return Ok("Deleted!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }   
}