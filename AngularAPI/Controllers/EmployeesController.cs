﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.DataAccess;
using Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AngularAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private DBTransactionContext _context;

        public EmployeesController(DBTransactionContext context)
        {
            _context = context;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Employees>> Get()
        {
            var list = _context.Employees;
            return list;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Employees> Get(int id)
        {
            return _context.Employees.Find(id);
        }
        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Employees employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.Employees.Add(employee);
            _context.SaveChanges();
            return Ok("Success!");
        }
        //PUT api/values/1
        [HttpPut]
        public IActionResult Put([FromBody] Employees employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            _context.Employees.Update(employee);
            _context.SaveChanges();
            return Ok("Updated!");
        }

        // GET api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var item = _context.Products.Find(id);
                _context.Products.Remove(item);
                _context.SaveChanges();

                return Ok("Deleted!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}