﻿using System;

namespace Api.Models
{
    public partial class Products
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public DateTime? Availability { get; set; }
        public int? Price { get; set; }
        public int? StarRating { get; set; }
        public string ImageUrl { get; set; }
    }
}
