﻿using System;
using Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Api.DataAccess
{
    public partial class DBTransactionContext : DbContext
    {
        public DBTransactionContext()
        {
        }

        public DBTransactionContext(DbContextOptions<DBTransactionContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<DailySheet> DailySheet { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Products>(entity =>
            {
                entity.Property(e => e.Availability).HasColumnType("datetime");

                entity.Property(e => e.ImageUrl).IsRequired();
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.Property(e => e.Dob).HasColumnType("datetime");

                entity.Property(e => e.ImageUrl).IsRequired();
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FirstName).IsRequired();
            });

            modelBuilder.Entity<Customers>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerID).IsRequired();
            });
        }
    }
}
