﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Models
{
    public class CustomerInvoice
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string MilkQuantity { get; set; }
        public string MilkFat { get; set; }
        public string Amount { get; set; }
        public string CreatedDate { get; set; }
    }
}
